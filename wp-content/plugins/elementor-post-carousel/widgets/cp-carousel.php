<?php
namespace CPCarousel\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Control_Media;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class CP_Carousel extends Widget_Base {

	public function get_name() {
		return 'custom-post-carousel';
	}

	public function get_title() {
		return __( 'Custom Post Carousel', 'custom_elementor' );
	}

	public function get_icon() {
		return 'eicon-slider-push';
	}

	public function get_categories() {
		return [ 'general-elements' ];
	}

	public function get_script_depends() {
		return [ 'jquery-slick' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_image_carousel',
			[
				'label' => __( 'Custom Post Carousel', 'elementor' ),
			]
		);

		$slides_to_show = range( 1, 4 );
		$slides_to_show = array_combine( $slides_to_show, $slides_to_show );

		$this->add_control(
			'slides_to_show',
			[
				'label' => __( 'Slides to Show', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'options' => $slides_to_show,
			]
		);

		$this->add_control(
			'slides_to_scroll',
			[
				'label' => __( 'Slides to Scroll', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => '2',
				'options' => $slides_to_show,
				'condition' => [
					'slides_to_show!' => '1',
				],
			]
		);

		$post_types = get_post_types();
		foreach ( get_post_types( '', 'names' ) as $post_type ) {
		   $pt[$post_type]=$post_type;
		}
		$this->add_control(
			'post_type',
			[
				'label' => __( 'Post Type', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => $pt,
			]
		);

		$this->add_control(
			'slide_id',
			[
				'label' => __( 'View', 'elementor' ),
				'type' => Controls_Manager::TEXT,
				'default' => '0',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'elementor' ),
			]
		);


		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'elementor' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'elementor' ),
					'no' => __( 'No', 'elementor' ),
				],
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'elementor' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
			]
		);



		$this->add_control(
			'speed',
			[
				'label' => __( 'Animation Speed', 'elementor' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => __( 'Navigation', 'elementor' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);


		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 20,
						'max' => 60,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image-carousel-wrapper .slick-slider .slick-prev:before, {{WRAPPER}} .elementor-image-carousel-wrapper .slick-slider .slick-next:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_color',
			[
				'label' => __( 'Arrows Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-image-carousel-wrapper .slick-slider .slick-prev:before, {{WRAPPER}} .elementor-image-carousel-wrapper .slick-slider .slick-next:before' => 'color: {{VALUE}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);


		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Dots Size', 'elementor' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 5,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-image-carousel-wrapper .elementor-image-carousel .slick-dots li button:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'dots_color',
			[
				'label' => __( 'Dots Color', 'elementor' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-image-carousel-wrapper .elementor-image-carousel .slick-dots li button:before' => 'color: {{VALUE}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();

		

	}

	protected function render() {

		$settings = $this->get_settings();
		$slide_id=$settings['slide_id'];

/*		if ( empty( $settings['carousel'] ) )
			return;*/

		$r = get_posts( array(
		   'no_found_rows' => true, /*suppress found row count*/
		   'post_status' => 'publish',
		   'post_type' => $settings['post_type'],
		   'ignore_sticky_posts' => true,
		   'meta_query'    =>  array(
		                                array(
		                                    'key'     => '_ita_featured', 
		                                    'value'   => 1,
		                                    'compare' => '=' 
		                                )
		                         )
		           )
		);

		$slides = [];

		if ( $r ) {

		    foreach ( $r as $post ) :
		    	    
		    
			if ( has_post_thumbnail($post->ID)) {
		
//Get the Thumbnail URL
                $image_url= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'scrolldim', false, '' );
			$image_html = '<img src="' . esc_attr( $image_url[0] ) . '" alt="' . esc_attr( Control_Media::get_image_alt( $attachment ) ) . '" width="100%" />';
			$link = get_permalink($post->ID);


			if ( $link ) {
				$image_html = sprintf( '<a href="%s">%s</a>', $link,  $image_html );
			}

			$image_caption = get_the_title($post->ID);

			$slide_html = '<li><figure class="postcarous">' . $image_html;

			if ( ! empty( $image_caption ) ) {
				$slide_html .= '<figcaption class="post-carousel-caption">' . $image_caption . '</figcaption>';
			}

			$slide_html .= '</figure></li>';

			$slides[] = $slide_html;
				}

			    endforeach; 
		}


		if ( empty( $slides ) ) {
			return;
		}

				$tch_viewport = 473;
		$tch_width = '95vw';
		$tch_height = 240;
		$tch_display = true;
		$tch_controls = true;
		$tch_interval = true;
		$tch_intervaltime = 3000;

		$tch_duration = 2000;

		$tch = $tch . "<style type='text/css' media='screen'>
.myelem_slider { height: 1%; margin: 30px 0 0; overflow:hidden; position: relative; padding: 0 0px 10px;   }
.myelem_slider .viewport { height: ".$tch_height."px; overflow: hidden; position: relative; width: 100% }
.myelem_slider .buttons { 
	background: #C01313; border-radius: 35px; 
	display: block; 
	position: absolute;
	top: 40%; left: 0; width: 35px; height: 35px; color: #fff; font-weight: bold; text-align: center; line-height: 35px; text-decoration: none;
font-size: 22px; }
.myelem_slider .next { right: 0; left: auto;top: 40%; }
.myelem_slider .buttons:hover { color: #C01313;background: #fff; }
.myelem_slider .disable { visibility: hidden; }
.myelem_slider .overview { list-style: none; position: absolute; padding: 0; margin: 0; width: 360px; left: 0 top: 0; }
.myelem_slider .overview li{ float: left; margin: 0 20px 0 0; padding: 1px; height: ".$tch_height."px; border: 1px solid #dcdcdc; width: ".$tch_width.";max-width: 360px;}
.post-carousel-caption {position: absolute; left: 10px; bottom: 15px;background-color: rgba(0,0,0,0.6);padding: 3px 8px;}
figure.postcarous {position: relative;}
</style>";
	
		$tch = $tch . '<div id="'.$slide_id.'" class="myelem_slider">';
			
			$tch = $tch . '<div class="viewport">';
				$tch = $tch . '<ul class="overview">';
					$tch = $tch . implode('',$slides);
				$tch = $tch . '</ul>';
			$tch = $tch . '</div>';
			$tch = $tch . '<a class="buttons prev" href="#">&#60;</a>';
			$tch = $tch . '<a class="buttons next" href="#">&#62;</a>';
		$tch = $tch . '</div>';
		
		$tch = $tch . '<script type="text/javascript">';
		$tch = $tch . 'jQuery(document).ready(function(){';
			$tch = $tch . "jQuery('#".$slide_id."').tinycarousel({ buttons: ".$tch_controls.", interval: ".$tch_interval.", intervalTime: ".$tch_intervaltime.", animationTime: ".$tch_duration." });";
		$tch = $tch . '});';
		$tch = $tch . '</script>';
	
	echo $tch;
		
	}

	private function get_link_url( $attachment, $instance ) {
		if ( 'none' === $instance['link_to'] ) {
			return false;
		}

		if ( 'custom' === $instance['link_to'] ) {
			if ( empty( $instance['link']['url'] ) ) {
				return false;
			}
			return $instance['link'];
		}

		return [
			'url' => wp_get_attachment_url( $attachment['id'] ),
		];
	}

	private function get_image_caption( $attachment ) {
		$caption_type = $this->get_settings( 'caption_type' );

		if ( empty( $caption_type ) ) {
			return '';
		}

		$attachment_post = get_post( $attachment['id'] );

		if ( 'caption' === $caption_type ) {
			return $attachment_post->post_excerpt;
		}

		if ( 'title' === $caption_type ) {
			return $attachment_post->post_title;
		}

		return $attachment_post->post_content;
	}
}
