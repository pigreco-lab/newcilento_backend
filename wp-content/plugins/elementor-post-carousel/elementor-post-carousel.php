<?php
/**
 * Plugin Name: Elementor Custom Post Carousel
 * Description: Custom element added to Elementor
 * Plugin URI: http://roberto-bruno.me
 * Version: 0.0.1
 * Author: Roberto Bruno
 * Author URI: http://roberto-bruno.me
 * Text Domain: elementor-custom-element
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'ELEMENTOR_CPC__FILE__', __FILE__ );

/**
 * Load Hello World
 *
 * Load the plugin after Elementor (and other plugins) are loaded.
 *
 * @since 1.0.0
 */
function cp_carousel_load() {
	// Load localization file
	load_plugin_textdomain( 'cp-carousel' );

	// Notice if the Elementor is not active
	if ( ! did_action( 'elementor/loaded' ) ) {
		add_action( 'admin_notices', 'cp_carousel_fail_load' );
		return;
	}

	// Check version required
	$elementor_version_required = '1.0.0';
	if ( ! version_compare( ELEMENTOR_VERSION, $elementor_version_required, '>=' ) ) {
		add_action( 'admin_notices', 'cp_carousel_fail_load_out_of_date' );
		return;
	}

	// Require the main plugin file
	require( __DIR__ . '/plugin.php' );
}
add_action( 'plugins_loaded', 'cp_carousel_load' );
