<?php
namespace MatthiasWeb\RealMediaLibrary\attachment;
use MatthiasWeb\RealMediaLibrary\general;

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * This class handles all hooks for the custom field in a attachment.s
 */
class CustomField extends general\Base {
    private static $me = null;
    
    private function __construct() {
        // Silence is golden.
    }
    
    /**
     * When editing a attachment show up a select
     * option to change the parent folder.
     * 
     * @hooked attachment_fields_to_edit
     */
    public function attachment_fields_to_edit($form_fields, $post) {
        $folderID = wp_attachment_folder($post->ID);
        
        // Check move permission
        $selectDisabled = "";
        if ($folderID > 0) {
            $folder = wp_rml_get_object_by_id($folderID);
            if (is_rml_folder($folder) && $folder->isRestrictFor("mov")) {
                $selectDisabled = 'disabled="disabled"';
            }
        }
        
        // Create form field
        $form_fields['rml_dir'] = array(
        	'label' => __('Folder', RML_TD),
        	'input' => 'html',
        	'html'  => 
        	    '<div class="rml-folder-edit" ' . $selectDisabled . '>' .
        	    Structure::getInstance()->getView()->getHTMLBreadcrumbByID($folderID) . '
        	       <select name="attachments[' . $post->ID . '][rml_folder]" ' . $selectDisabled . '>
	                        ' . Structure::getInstance()->optionsFasade($folderID, null, false) . '
        	       </select>
        	    </div>',
        );
        return $form_fields;
    }
    
    /**
     * When saving a attachment change the parent folder.
     * 
     * @hooked attachment_fields_to_save
     */
    public function attachment_fields_to_save($post, $attachment) {
        if (isset($attachment['rml_folder'])){
            if (Structure::getInstance()->getFolderByID($attachment['rml_folder']) === null) {
                $attachment['rml_folder'] = _wp_rml_root();
            }
            // Get previous folder id
            $updateCount = array(wp_attachment_folder($post["ID"]), $attachment["rml_folder"]);
            
            // Update to new folder id
            $result = wp_rml_move($attachment['rml_folder'], $post['ID']);
            if (is_array($result)) {
                $post['errors']['rml_folder']['errors'][] = implode(" ", $result);
            }
            
            // Reset the count of both folders manually because we do not use the wp_rml_move api method
            CountCache::getInstance()->resetCountCache($updateCount);
        }
        
        return $post;
    }
    
    public static function getInstance() {
        if (self::$me == null) {
            self::$me = new CustomField();
        }
        return self::$me;
    }
}