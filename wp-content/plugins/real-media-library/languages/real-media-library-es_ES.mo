��    )      d      �      �  m   �  H     ~   d     �  	   �  $   �          $     4     ;     C     Z     q     x  	   �     �     �  =   �     �     �       
              '  H   ,  4   u     �     �     �     �     �     �     �  	             !  1   *     \     v     �  �  �  q   0
  W   �
  �   �
     �     �  4   �     �     �     �                    9     A     J      [     |  C   �     �     �     �     �            S   "  4   v     �     �     �     �     �     �     �  	             2  <   @  &   }     �     �   A collection can contain no files. But you can create there other collections and <strong>galleries</strong>. A folder can contain every type of file or a collection, but no gallery. A gallery can contain only images. If you want to display a gallery go to a post and have a look at the visual editor buttons. All All Files Allow all folders for folder gallery Are you sure? Attachment File Cancel Columns Do not delete root. :( Do not rename root. :( Folder Folders Full Size Gallery from Media Folder Hide upload preview In this folder are sub directories, please delete them first! Large Link to Matthias Günter Media File Medium None Note: You can only select galleries. Folders and collections are grayed. Organize your wordpress media library in a nice way. Random Order Rename folder Save Select Size Something went wrong. Tell me the new name Thumbnail WP Real Media Library Wipe all Wipe all settings (folders, attachment relations) Wipe attachment relations Yes, delete it! http://matthias-web.de Project-Id-Version: real-media-library
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-31 13:53+0200
PO-Revision-Date: Fri Feb 10 2017 10:31:18 GMT+0100 (Mitteleuropäische Zeit)
Last-Translator: admin <matthias_guenter@freenet.de>
Language-Team: 
Language: Spanish (Spain)
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Generator: Loco - https://localise.biz/
X-Poedit-Basepath: .
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Poedit-SearchPath-0: ..
X-Loco-Target-Locale: es_ES Una colección no puede contener archivos. Pero puede crear allí otras colecciones y <strong>galerías</strong>. Una carpeta puede contener cada tipo de archivo o una colección, pero no una galería. Una galería solo puede contener imágenes. Si desea mostrar una galería, vaya a una entrada y eche un vistazo a los botones del editor visual. Todas Todos los archivos Permitir todas las carpetas para galería de carpeta ¿Está seguro? Archivo adjunto Cancelar Columnas No borrar raíz. :( No renombrar la raíz. :( Carpeta Carpetas Tamaño completo Galería de la carpeta de medios Ocultar vista previa de carga En esta carpeta hay subdirectorios. Por favor, primero elimínelos. Grande Enlace a Matthias Günter Archivo multimedia Mediano Ninguno Nota: solo puede seleccionar galerías. Las carpetas y colecciones están grisadas. Organice mejor su biblioteca de medios de WordPress. Orden aleatorio Renombrar carpeta Guardar Seleccionar Tamaño Algo salió mal. Decirme el nombre nuevo Miniatura WP Real Media Library Limpiar todas Limpiar todos los ajustes (carpetas, relaciones de adjuntos) Limpiar las relaciones de los adjuntos ¡Sí, borrarla! http://matthias-web.de 