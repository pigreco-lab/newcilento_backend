<?php
/**
 * Plugin Name: 	PL Custom Role
 * Plugin URI:		http://special-themes.com/
 * Description:		A Woocommerce plugin for adding a shortcode [woocommerce_last_orders] which shows the last orders (if any) of the logged user
 * Version: 		1.0.0
 * Author: 		Pigrecolab
 * Author URI: 		http://special-themes.com/
 * Text Domain: 	woocommerce-last-order
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class PL_Custom_Role.
 *
 * Main PL_Custom_Role class initializes the plugin.
 *
 * @class		PL_Custom_Role
 * @version		1.0.0
 * @author		Roberto Bruno
 */
class PL_Custom_Role {


	/**
	 * Plugin version. 
	 *
	 * @since 1.0.0
	 * @var string $version Plugin version number.
	 */
	public $version = '1.0.0';

		/**
	 * Role slug version.
	 *
	 * @since 1.0.0
	 * @var string $version Plugin version number.
	 */
	public $roleslug = 'accomodation_manager';

			/**
	 * Role slug version.
	 *
	 * @since 1.0.0
	 * @var string $version Plugin version number.
	 */
	public $rolename ='Accomodation Manager' ;

	/**
	 * Instance of PL_Custom_Role.
	 *
	 * @since 1.0.0
	 * @access private
	 * @var object $instance The instance of PL_Custom_Role.
	 */
	private static $instance;


	/**
	 * Construct.
	 *
	 * Initialize the class and plugin.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Initialize plugin parts
		$this->init();

		register_activation_hook( __FILE__,  array( $this,'add_role' ));
		register_deactivation_hook( __FILE__,  array( $this,'remove_role' ));
		add_action( 'admin_init', array( $this,'remove_menu_pages' ));
		//add_action( 'load-edit.php', array( $this,'posts_for_current_role' ));

		// fix the 6-years-old bug on author dropdown
		add_action( 'load-post.php',     array( $this,'th_load_user_dropdown_filter' ));
		add_action( 'load-post-new.php', array( $this,'th_load_user_dropdown_filter' ));

		add_action( 'admin_bar_menu', array( $this,'manage_admin_bar_elements'), 999 );

		//redirect for accomodation manager role 
		add_filter('login_redirect', array( $this,'my_login_redirect'), 10, 3);


		//ADD THE ACCOMODATION AND  PLACE CPT
		add_action( 'init', array( $this,'register_cpt_itrav' ));
		//add_action( 'init', array( $this,'load_custom_search_template' ));
		/****** CUSTOM POST COLUMN ****/
		add_filter('manage_edit-place_columns', array( $this,'place_columns'));
		add_action('manage_place_posts_custom_column', array( $this,'place_custom_columns'), 10, 2);
		add_filter('manage_edit-accomodation_columns', array( $this,'accomodation_columns'));
		add_action('manage_accomodation_posts_custom_column', array( $this,'accomodation_custom_columns'), 10, 2);

		/****** PLACES FEATURED CHANGE STATUS ****/

	global $pagenow,$typenow; //&& $typenow =='page'

	 if (is_admin()   && $typenow='place'){
	                add_action( 'admin_footer', array( $this,'changefeat_javascript' )); // Write our JS below here
	                add_action( 'wp_ajax_changefeat', array( $this,'changefeat_callback' ));
            }

		add_action( 'admin_init', array( $this,'add_cpt_caps'));

		add_filter( 'redirect_post_location', array( $this,'redirect_post_location' ));

		add_shortcode( 'itrav_advanced_search', array( $this,'search_short' ));
	}


	/**
	 * Instance.
	 *
	 * An global instance of the class. Used to retrieve the instance
	 * to use on other files/plugins/themes.
	 *
	 * @since 1.0.0
	 * @return object Instance of the class.
	 */
	public static function instance() {

		if ( is_null( self::$instance ) ) :
			self::$instance = new self();
		endif;

		return self::$instance;

	}


	/**
	 * init.
	 *
	 * Initialize plugin parts.
	 *
	 * @since 1.0.0
	 */
	public function init() {

		// Load textdomain
		$this->load_textdomain();

	}


	/**
	 * Textdomain.
	 *
	 * Load the textdomain based on WP language.
	 *
	 * @since 1.0.0
	 */
	public function load_textdomain() {

		// Load textdomain
		load_plugin_textdomain( 'pl-custom-role', false, basename( dirname( __FILE__ ) ) . '/languages' );

	}

	public function add_role() { 

		if ($this->role_exists($this->roleslug)) return;

		add_role(
    		$this->roleslug,
   		 __( $this->rolename ),
		    array(
		        'read'         => true,  // true allows this capability
		        'edit_published_accomodations' => true,
		        'edit_accomodations' => true,
		        'publish_accomodations' => true,
		        'publish_accomodation' => true,
		        'edit_other_accomodations' => true,
		        'unfiltered_html' => true,
		    )
		);
	}

	public function remove_role() { 
		//check if role exist before removing it
		$role=get_role($this->roleslug);
		if( $role) {
		      remove_role( $this->roleslug );
		}
	}

	public function role_exists( $role ) {

	  if( ! empty( $role ) ) {
	    return $GLOBALS['wp_roles']->is_role( $role );
	  }
	  
	  return false;
	}

	public function remove_menu_pages() {

	    if ( current_user_can( $this->roleslug ) ) {

	    	global $submenu;
		  unset($submenu['edit.php?post_type=page'][10]);

		//echo '<style type="text/css">	  #adminmenuback, #adminmenuwrap { display:none; }		</style>';

		// Hide link on listing page and the entire menu
		if (isset($_GET['post_type']) && $_GET['post_type'] == 'page') {
/*		    echo '<style type="text/css">
		    .page-title-action { display:none; }
		    </style>';*/
		}

		//remove some links from the code
		remove_menu_page('edit-comments.php'); // Comments
		remove_menu_page('edit.php'); // Posts
		remove_menu_page('tools.php'); // Tools

	    }

	    // Semplificazione menu per Alessandro

	   if( 2 == get_current_user_id()) {
		//remove some links from the code
		remove_menu_page('edit-comments.php'); // Comments
		remove_menu_page('edit.php'); // Posts
		remove_menu_page('tools.php'); // Tools
		remove_menu_page( 'themes.php' );                 //Appearance
  		remove_menu_page( 'plugins.php' );                //Plugins
  		remove_menu_page( 'edit.php?post_type=acf' );                //ACF
  		remove_menu_page( 'edit.php?post_type=booking-system' );  
  		remove_menu_page( 'aps-social' );         
  		remove_menu_page( 'elementor' );         
  		remove_menu_page( 'ap-twitter-feed' );
  		remove_menu_page( 'site-migration-export' );
  		remove_menu_page( 'options-general.php' );     
 		remove_menu_page( 'index.php' );                  //Dashboard 
	   }
	}



		public function manage_admin_bar_elements( $wp_admin_bar ) {
			$wp_admin_bar->remove_node( 'wp-logo' );
			$wp_admin_bar->remove_node( 'new-content' );
			$wp_admin_bar->remove_node( 'comments' );
			$wp_admin_bar->remove_node( 'menu-toggle' );
			$wp_admin_bar->remove_node( 'logout' );

			$pagepost = get_posts("post_type=accomodation&author=". get_current_user_id()."&numberposts=1");
			$pageid=$pagepost[0]->ID;

			$wp_admin_bar->add_node( array(
			'parent'	=> 'user-actions',
			'id'		=> 'page-editor',
			'title'		=> '<span class="ab-icon"></span><span class="ab-label">' . __( 'Edit page' ) . '</span>',
			'href'		=> admin_url( 'post.php?post='.$pageid.'&action=edit' )
		) );


			$wp_admin_bar->add_node( array(
			'parent'	=> 'user-actions',
			'id'		=> 'calendar-editor',
			'title'		=> '<span class="ab-icon"></span><span class="ab-label">' . __( 'Edit calendar' ) . '</span>',
			'href'		=> admin_url( 'post.php?post='.$pageid.'&action=edit' )
		) );


			$wp_admin_bar->add_node( array(
			'parent'	=> 'user-actions',
			'id'		=> 'logout',
			'title'		=> '<span class="ab-icon"></span><span class="ab-label">' . __( 'Log Out' ) . '</span>',
			'href'		=> wp_logout_url()
		) );
		}

	public function my_login_redirect($redirect_to, $request, $user) {
			$role=$user->roles;
	  if ( "accomodation_manager"==$role[0] ) {
			$pagepost = get_posts("post_type=accomodation&author=". get_current_user_id()."&numberposts=1");
			$pageid=$pagepost[0]->ID;
	  		return admin_url( 'post.php?post='.$pageid.'&action=edit' );
	  	die;
		}
		return $redirect_to;
	}


	public function register_cpt_itrav() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Types', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Type', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Types', 'textdomain' ),
		'all_items'         => __( 'All Types', 'textdomain' ),
		'parent_item'       => __( 'Parent Type', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Type:', 'textdomain' ),
		'edit_item'         => __( 'Edit Type', 'textdomain' ),
		'update_item'       => __( 'Update Type', 'textdomain' ),
		'add_new_item'      => __( 'Add New Type', 'textdomain' ),
		'new_item_name'     => __( 'New Type Name', 'textdomain' ),
		'menu_name'         => __( 'Type', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels, 
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'type' ),
	);

	register_taxonomy( 'type', 'accomodation', $args );


		$labels = array( 
		    'name' => __( 'Accomodations', 'accomodation' ),
		    'singular_name' => __( 'Accomodation', 'accomodation' ),
		    'add_new' => __( 'Add New', 'accomodation' ),
		    'add_new_item' => __( 'Add New Accomodation', 'accomodation' ),
		    'edit_item' => __( 'Edit Accomodation', 'accomodation' ),
		    'new_item' => __( 'New Accomodation', 'accomodation' ),
		    'view_item' => __( 'View Accomodation', 'accomodation' ),
		    'search_items' => __( 'Search Accomodations', 'accomodation' ),
		    'not_found' => __( 'No accomodations found', 'accomodation' ),
		    'not_found_in_trash' => __( 'No accomodations found in Trash', 'accomodation' ),
		    'parent_item_colon' => __( 'Parent Accomodation:', 'accomodation' ),
		    'menu_name' => __( 'Accomodations', 'accomodation' ),
		);

		$args = array( 
		    'labels' => $labels,
		    'hierarchical' => true,
		    'description' => 'Travel Accomodations',
		    'supports' => array( 'title', 'editor', 'author','thumbnail','excerpt','revisions'),
		    'taxonomies'          => array( 'types' ),
		    'public' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'menu_icon' => 'dashicons-clipboard',
		    'show_in_nav_menus' => true,
		    'publicly_queryable' => true,
		    'exclude_from_search' => false,
		    'has_archive' => true,
		    'query_var' => true,
		    'can_export' => true,
		    'rewrite' => true,
		    'capabilities' => array(
		        'edit_post' => 'edit_accomodation',
		        'edit_posts' => 'edit_accomodations',
		        'edit_others_posts' => 'edit_other_accomodations',
		        'edit_published_posts' => 'edit_published_accomodations',
		        'publish_posts' => 'publish_accomodations',
		        'read_post' => 'read_accomodation',
		        'read_private_posts' => 'read_private_accomodations',
		        'delete_post' => 'delete_accomodation'
		    ),
		    // as pointed out by iEmanuele, adding map_meta_cap will map the meta correctly 
		    'map_meta_cap' => true
		);

		register_post_type( 'accomodation', $args );

		$labels = array( 
		    'name' => __( 'Places', 'accomodation' ),
		    'singular_name' => __( 'Place', 'accomodation' ),
		    'add_new' => __( 'Add New', 'accomodation' ),
		    'add_new_item' => __( 'Add New Place', 'accomodation' ),
		    'edit_item' => __( 'Edit Place', 'accomodation' ),
		    'new_item' => __( 'New Place', 'accomodation' ),
		    'view_item' => __( 'View Place', 'accomodation' ),
		    'search_items' => __( 'Search Places', 'accomodation' ),
		    'not_found' => __( 'No places found', 'accomodation' ),
		    'not_found_in_trash' => __( 'No Places found in Trash', 'accomodation' ),
		    'parent_item_colon' => __( 'Parent Place:', 'accomodation' ),
		    'menu_name' => __( 'Places', 'accomodation' ),
		);

		$args = array( 
		    'labels' => $labels,
		    'hierarchical' => true,
		    'description' => 'Travel Places',
		    'supports' => array( 'title', 'editor', 'author','thumbnail','excerpt','revisions'),
		    'public' => true,
		    'show_ui' => true,
		    'show_in_menu' => true,
		    'menu_icon' => 'dashicons-location',
		    'show_in_nav_menus' => true,
		    'publicly_queryable' => true,
		    'exclude_from_search' => false,
		    'has_archive' => true,
		    'query_var' => true,
		    'can_export' => true,
		    'rewrite' => true
		);

		register_post_type( 'place', $args );
}


	public function place_columns($columns){
	$columns = array(
		'cb' => '<input type="checkbox" />',

		'title' => __( 'Place', 'itrav'  ),
		'thumbs' => __('Thumbs', 'itrav' ),
		'featured' => __( 'Featured', 'itrav'  )


		);

	return $columns;

}

	public function place_custom_columns($column, $post_id){
		global $post;


		$prefix="_cde_";

		switch( $column ) {

			/* If displaying the 'thumb' column. */
			case 'thumbs' :

			echo the_post_thumbnail( array(80,80) );

			break;

			case 'featured' :
				$feat= get_post_meta( $post_id, '_ita_featured', true );

				echo ($feat==0) ? '<span class="dashicons dashicons-star-empty pb" pid="'.$post_id.'"></span>' : '<span class="dashicons dashicons-star-filled pb" pid="'.$post_id.'"></span>';
			break;
			

			/* Just break out of the switch statement for everything else. */
			default :
			break;
		}

	}

	public function accomodation_columns($columns){
	$nw_columns = array(

		'ref' => __('Ref.', 'itrav' ),
		'thumbs' => __('Thumbs', 'itrav' ),
		'featured' => __( 'Featured', 'itrav'  ),
		'ID' => 'ID'

		);

	return array_merge($columns,$nw_columns);

}

	public function accomodation_custom_columns($column, $post_id){
		global $post;


		switch( $column ) {

			/* If displaying the 'thumb' column. */
			case 'thumbs' :

			echo the_post_thumbnail( array(80,80) );

			break;


			case 'ID' :

				echo $post_id;

			break;

			case 'ref' :

				echo get_post_meta( $post_id, 'rif', true );

			break;

			case 'featured' :
				$feat= get_post_meta( $post_id, '_ita_featured', true );

				echo ($feat==0) ? '<span class="dashicons dashicons-star-empty pb" pid="'.$post_id.'"></span>' : '<span class="dashicons dashicons-star-filled pb" pid="'.$post_id.'"></span>';
			break;
			

			/* Just break out of the switch statement for everything else. */
			default :
			break;
		}

	}

	public function changefeat_javascript() { 
							$icn='<img id="animgif" src="'.site_url().'/wp-admin/images/wpspin_light.gif" style="float:left">';
		?>
            
	<script type="text/javascript">
	function chf(p){
                		
                		params={};
						params.post_id=p.attr("pid");
						params.action="changefeat";//IMPORTANT: THE VALUE ACTION SHOULD BE THE SAME IN 'WP_AJAX_...'
                		jQuery.post(ajaxurl,params, 
                    function(response) {
                        if (response.error){
                            alert(response.error);                      
                        }else{
                            p.toggleClass("dashicons dashicons-star-empty dashicons dashicons-star-filled");
                            jQuery("#animgif").remove();
                        }
                    }
                );
                
            }

	jQuery(document).ready(function($) {


		  jQuery(".pb").click(function(){
		  			$(this).prepend('<?php echo $icn ?>')
                    chf(jQuery(this));
                });

	});
	</script> <?php
}



        //ajax callback function
        public function changefeat_callback() {
      
            if (!isset($_POST['post_id'])){
                $re = 'something went wrong ...';
                echo json_encode($re);
                die();
            }
            if (isset($_POST['action'])){

                $this->change_feat_db($_POST['post_id']);

            }else{
                $re = 'something went wrong ...';
            }
              wp_die(); // this is required to terminate immediately and return a proper response
        }


        public function change_feat_db($psid){
        	$prefix="_ita_";
            $current_meta = get_post_meta( $psid);
           $newval =  1-intval($current_meta[$prefix.'featured'][0]);
            update_post_meta($psid, $prefix.'featured',$newval  );
        }


		public function add_cpt_caps() {
		    // gets the administrator role
		    $admins = get_role( 'administrator' );

		    $admins->add_cap( 'edit_accomodation' ); 
		    $admins->add_cap( 'edit_accomodations' ); 
		    $admins->add_cap( 'edit_other_accomodations' ); 
		    $admins->add_cap( 'edit_published_accomodations' ); 
		    $admins->add_cap( 'publish_accomodations' ); 
		    $admins->add_cap( 'read_accomodation' ); 
		    $admins->add_cap( 'read_private_accomodations' ); 
		    $admins->add_cap( 'delete_accomodation' ); 
		}

		public function redirect_post_location( $location ) {

		    if ( 'accomodation' == get_post_type() ) {

		    /* Custom code for 'accomodation' post type. */

		        if ( isset( $_POST['save'] ) || isset( $_POST['publish'] ) )
		            return admin_url( "edit.php?post_type=accomodation" );

		    } 
		    return $location;
		} 

	public function posts_for_current_role() {
	    global $user_ID;

	    if ( current_user_can($this->roleslug ) ) {
	       if ( ! isset( $_GET['author'] ) ) {
	          wp_redirect( add_query_arg( 'author', $user_ID ) );
	          exit;
	       }
	   }

	}


	public function th_load_user_dropdown_filter() {
	    $screen = get_current_screen();

	    if ( empty( $screen->post_type ) || 'accomodation' !== $screen->post_type )
	        return;

	    add_filter( 'wp_dropdown_users_args', array($this,'th_dropdown_users_args'),10,2  );
	}

	public function th_dropdown_users_args( $args, $r ) {
	    global $wp_roles, $post;

	    // Check that this is the correct drop-down.
	    if ( 'post_author_override' === $r['name'] && 'accomodation' === $post->post_type ) {

	            $args['who']      = '';
	            $args['role__in'] = $this->roleslug;
	        }


	    return $args;
	}

	public function load_custom_search_template(){
	    if( isset($_REQUEST['search']) == 'advanced' ) {

				if ( $theme_file = locate_template( array ( 'advanced-results.php' ) ) ) {
					$template_path = $theme_file;
				} else {
					$template_path = plugin_dir_path( __FILE__ ) . '/templates/advanced-results.php';
				}
			require($template_path);
	        die();

		}
    }

	public function search_short() {

		$advshrt='<form method="get" id="advanced-searchform" role="search" action="'.esc_url( home_url( '/' ) ).'">


		    <!-- PASSING THIS TO TRIGGER THE ADVANCED SEARCH RESULT PAGE FROM functions.php -->
		    <input type="hidden" name="search" value="advanced">

		    <br /><label for="name" class="">'. __( 'Name: ', 'cde_pgl' ).'</label><br>
		    <input type="text" value="" placeholder="'. __( 'Type the Accomodation Name', 'cde_pgl' ).'" name="name" id="name" />

		        <br /><label for="regione" class="">'. __( 'Region: ', 'cde_pgl' ).'</label><br>
				<select id="regione" class="select" name="regione">
					<option value="Valle d\'Aosta">Valle d\'Aosta</option>
					<option value="Piemonte">Piemonte</option>
					<option value="Liguria">Liguria</option>
					<option value="Lombardia">Lombardia</option><option value="Veneto">Veneto</option>
					<option value="Trentino Alto Adige">Trentino Alto Adige</option>
					<option value="Friuli Venezia Giulia">Friuli Venezia Giulia</option>
					<option value="Emilia Romagna">Emilia Romagna</option>
					<option value="Toscana">Toscana</option>
					<option value="Umbria">Umbria</option>
					<option value="Marche">Marche</option>
					<option value="Lazio">Lazio</option>
					<option value="Abruzzo">Abruzzo</option>
					<option value="Molise">Molise</option>
					<option value="Campania">Campania</option>
					<option value="Puglia">Puglia</option>
					<option value="Basilicata">Basilicata</option>
					<option value="Calabria">Calabria</option>
					<option value="Sicilia">Sicilia</option>
					<option value="Sardegna">Sardegna</option>
				</select>';
			

		    $advshrt.= '<br /><label for="tipo" class="">'. __( 'Accomodation type: ', 'cde_pgl' ).'</label><br>
		    <select id="acc_type" class="select" name="tipo">
		    	<option value="beb:B&amp;B">beb:B&amp;B</option>
		    	<option value="affittacamere">affittacamere:Affittacamere</option>
		    	<option value="hotel">hotel:Hotel</option>
		    	<option value="casavacanza">Casa Vacanza</option>
		    	<option value="camping">Camping</option>
		    	<option value="" selected="selected"></option>
		    </select>



		    <input type="submit" id="searchsubmit" value="Search" />

		</form>';
		
		return $advshrt;
	}

}


/**
 * The main function responsible for returning the PL_Custom_Role object.
 *
 * Use this function like you would a global variable, except without needing to declare the global.
 *
 * Example: <?php PL_Custom_Role()->method_name(); ?>
 *
 * @since 1.0.0
 *
 * @return object PL_Custom_Role class object.
 */
if ( ! function_exists( 'PL_Custom_Role' ) ) :

 	function PL_Custom_Role() {

/*		if( is_admin() ) {
			require_once ("classes/wsf_Options.php");
			$wsfopt = new wsf_Options();

			
		}*/

    	return PL_Custom_Role::instance();
	}

endif;

PL_Custom_Role();

