<?php

class WpDBConn  // dichiarazione di una classe per la connessione al database
{
	var $con;
	function WpDBConn()  {// costruttore

		include_once '../../../wp-config.php';
		// Definizione dei dati di connessione
		$conection['server']=DB_HOST;  //host
		$conection['user']=DB_USER; //$conection['user']="cafe";         //  nome utente
		$conection['pass']=DB_PASSWORD; //$conection['pass']="C#$fE12";             //password
		$conection['base']=DB_NAME;           //database
		
		// crea la connessione passando i dati
		$conect= @mysql_connect($conection['server'],$conection['user'],$conection['pass']);

		if ($conect) // se connessione avvenuta, collega il database
		{
			mysql_select_db($conection['base']);			
			$this->con=$conect;
				//CAMBIO IL TIMEZONE DI MYSQL
 			mysql_query("set time_zone = '+01:00'");

		}
	}
	function getCon() // restituisce la connessione
	{
		return $this->con;
	}
	function Close()  // chiude la connessione
	{
		mysql_close($this->con);
	}	

}

class sQuery   // classe per eseguire una query, richiama la classe precedente
{

	var $conn;
	var $query;
	var $risultati;
	function sQuery()  // constructor, crea una query usando la classe connessione
	{
		$this->conn= new WpDBConn();
	}
	function executeQuery($cons)  // esegue la query
	{
		$this->query= mysql_query($cons,$this->conn->getCon()) or die(mysql_error());
		return $this->query;
	}	
	
	
	function Close()		// chiude la connessione
	{$this->conn->Close();}	
	
	function Clean() // pulisce la query
	{mysql_free_result($this->query);}
	
    
}