<?php

class Application
{ 
	public $vars;

	function Application($file="vars.txt") { 
		$this->file=$file; 
		$this->load(); 
	} 

	function load(){ 
		if (file_exists($this->file)) 
		$this->vars = unserialize(file_get_contents($this->file)); 
	} 

	function save(){ 

		if (file_exists($this->file)){ 
			$fp = fopen($this->file, "w+"); 
			fwrite($fp, serialize($this->vars)); 
		 	fclose($fp); 
			$this->load(); 
		}
	} 

	function reset(){ 
		unset($this->vars); 
		$this->save(); 
	} 

	function set($var,$val){ 
		$this->vars[$var] = $val; 
		$this->save(); 
	} 

	function get($var){ 
		return $this->vars[$var]; 
	} 

} 

?>