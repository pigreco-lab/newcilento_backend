<?php
class Wdp_Options
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );

        add_filter( 'pre_update_option_wdp_options', array( $this,'add_wdp_vars'), 10, 1 );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        add_menu_page( 
            'Direct Print',
            'Direct Print',
            'manage_woocommerce',
             'direct_print_option'
             );
        add_submenu_page(
            'direct_print_option.php', 
            __( 'Direct Print Settings', 'woo-direct-print' ), 
            __( 'Direct Print Settings', 'woo-direct-print' ),
             'manage_woocommerce',
              'direct_print_option', 
              array(&$this, 'create_admin_page')
            );

    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'wdp_options' );
        ?>
        <div class="wrap">
            <h2><?php _e( 'Direct Print Settings', 'woo-direct-print' ) ?></h2>
           <?php if ( ! isset( $_REQUEST['settings-updated'] ) )
              $_REQUEST['settings-updated'] = false; ?>
 
 
          <?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
               <div class="updated fade"><p><strong><?php _e( 'WDP Options saved!', 'woo-direct-print' ); ?></strong></p></div>
          <?php endif; ?>
           
           
            <div id="poststuff">
                <div id="post-body">
                    <div id="post-body-content">
                         <form method="post" action="options.php">
                            <?php
                                // This prints out all hidden setting fields
                                settings_fields( 'wdp_options' );
                                do_settings_sections( 'wdp-setting-admin' );
                                do_settings_sections( 'wdp_skebby_page' );
                                submit_button();
                            ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting('wdp_options', 'wdp_options', array( $this, 'sanitize' ));

        add_settings_section(
            'wdp_section', // ID
            'Impostazioni messaggi', // Title
            array( $this, 'wdp_info' ), // Callback
            'wdp-setting-admin' // Page
        ); 

        add_settings_field(
            'wdp_code', // ID
            'Codice WDP', // Title 
            array( $this, 'wdp_code_callback' ), // Callback
            'wdp-setting-admin', // Page
            'wdp_section' // Section           
        );      

        add_settings_field(
            'wdp_sitename', 
            'Nome del sito', 
            array( $this, 'wdp_sitename_callback' ), 
            'wdp-setting-admin', 
            'wdp_section'
        ); 
        add_settings_field(
            'wdp_ownnum', 
            'NUmero proprietario', 
            array( $this, 'wdp_ownnum_callback' ), 
            'wdp-setting-admin', 
            'wdp_section'
        );   
        add_settings_field(
            'wdp_msgok', 
            'Messaggio di conferma', 
            array( $this, 'wdp_msgok_callback' ), 
            'wdp-setting-admin', 
            'wdp_section'
        );  
        add_settings_field(
            'wdp_msgdoh', 
            'Messaggio di rifiuto', 
            array( $this, 'wdp_msgdoh_callback' ), 
            'wdp-setting-admin', 
            'wdp_section'
        );     

        add_settings_section(
            'wdp_skebby', // ID
            'Impostazioni Skebby', // Title
            array( $this, 'wdp_skebby_info' ), // Callback
            'wdp_skebby_page' // Page
        ); 

        add_settings_field(
            'wdp_sk_username', 
            'Nome utente', 
            array( $this, 'wdp_sk_username_callback' ), 
            'wdp_skebby_page', 
            'wdp_skebby'
        ); 
        add_settings_field(
            'wdp_sk_pass', 
            'Password', 
            array( $this, 'wdp_sk_pass_callback' ), 
            'wdp_skebby_page', 
            'wdp_skebby'
        );   
        add_settings_field(
            'wdp_sk_sender', 
            'Stringa mittente', 
            array( $this, 'wdp_sk_sender_callback' ), 
            'wdp_skebby_page', 
            'wdp_skebby'
        );  
        add_settings_field(
            'wdp_sk_num', 
            'Numero inviante', 
            array( $this, 'wdp_sk_num_callback' ), 
            'wdp_skebby_page', 
            'wdp_skebby'
        );
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['id_number'] ) )
            $new_input['id_number'] = absint( $input['id_number'] );

        if( isset( $input['title'] ) )
            $new_input['title'] = sanitize_text_field( $input['title'] );

        //return $new_input;
        return $input;
    }

    public function add_wdp_vars($wdpopt) {
        
        $opt_ext = '<?php '."\n".
        '$code="'. $wdpopt["wdp_code"] .'";'."\n".
        '$website="'. $wdpopt["wdp_sitename"] .'";'."\n".
        '$ownernum="'. $wdpopt["wdp_ownnum"] .'";'."\n".
        '$msgok="'. $wdpopt["wdp_msgok"] .'";'."\n".
        '$msgdoh="'. $wdpopt["wdp_msgdoh"] .'";'."\n".
        '$sk_user="'. $wdpopt["wdp_sk_username"] .'";'."\n".
        '$sk_pass="'. $wdpopt["wdp_sk_pass"] .'";'."\n".
        '$sk_sender="'. $wdpopt["wdp_sk_sender"] .'";'."\n".
        '$sk_num="'. $wdpopt["wdp_sk_num"] .'";'."\n".
        '?>';

        $file = dirname(__DIR__) . '/code.php';
        //wp_die($file); 

        $open = fopen( $file, "w" ); 
        $write = fputs( $open, $opt_ext ); 
        fclose( $open );


        return $wdpopt;
    }

    /** 
     * Print the Section text
     */
    public function wdp_info()
    {
        print 'Inserisci qui le informazioni relative al sito e al codice';
    }

        public function wdp_skebby_info()
    {
        print 'Inserisci qui le informazioni relative a Skebby';
    }
    /** 
     * Get the settings option array and print one of its values
     */
    public function wdp_code_callback()
    {
        printf(
            '<input type="text" id="wdp_code" name="wdp_options[wdp_code]" value="%s" />',
            isset( $this->options['wdp_code'] ) ? esc_attr( $this->options['wdp_code']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function wdp_sitename_callback()
    {
        printf(
            '<input type="text" id="wdp_sitename" name="wdp_options[wdp_sitename]" value="%s" />',
            isset( $this->options['wdp_sitename'] ) ? esc_attr( $this->options['wdp_sitename']) : ''
        );
    }

    public function wdp_ownnum_callback()
    {
        printf(
            '<input type="text" id="wdp_ownnum" name="wdp_options[wdp_ownnum]" value="%s" />',
            isset( $this->options['wdp_ownnum'] ) ? esc_attr( $this->options['wdp_ownnum']) : ''
        );
    }
   public function wdp_msgok_callback()
    {
        printf(
            '<textarea id="wdp_sitename" name="wdp_options[wdp_msgok]" cols="30" rows="5">%s</textarea><br /><span class="description">%s</span>',
            isset( $this->options['wdp_msgok'] ) ? esc_attr( $this->options['wdp_msgok']) : '',
            'Puoi usare le seguenti variabili: [num_ord],[totale],[sitoweb],[numprop]'
        );
    }
  public function wdp_msgdoh_callback()
    {
        printf(
            '<textarea id="wdp_sitename" name="wdp_options[wdp_msgdoh]" cols="30" rows="5">%s</textarea><br /><span class="description">%s</span>',
            isset( $this->options['wdp_msgdoh'] ) ? esc_attr( $this->options['wdp_msgdoh']) : '',
            'Puoi usare le seguenti variabili: [num_ord],[totale],[sitoweb],[numprop]'
        );
    }

    public function wdp_sk_username_callback()
    {
        printf(
            '<input type="text" id="wdp_sk_username" name="wdp_options[wdp_sk_username]" value="%s" />',
            isset( $this->options['wdp_sk_username'] ) ? esc_attr( $this->options['wdp_sk_username']) : ''
        );
    }

    public function wdp_sk_pass_callback()
    {
        printf(
            '<input type="text" id="wdp_sk_pass" name="wdp_options[wdp_sk_pass]" value="%s" />',
            isset( $this->options['wdp_sk_pass'] ) ? esc_attr( $this->options['wdp_sk_pass']) : ''
        );
    }
    public function wdp_sk_sender_callback()
    {
        printf(
            '<input type="text" id="wdp_sk_sender" name="wdp_options[wdp_sk_sender]" value="%s" />',
            isset( $this->options['wdp_sk_sender'] ) ? esc_attr( $this->options['wdp_sk_sender']) : ''
        );
    }

    public function wdp_sk_num_callback()
    {
        printf(
            '<input type="text" id="wdp_sk_num" name="wdp_options[wdp_sk_num]" value="%s" />',
            isset( $this->options['wdp_sk_num'] ) ? esc_attr( $this->options['wdp_sk_num']) : ''
        );
    }

}