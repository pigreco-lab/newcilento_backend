jQuery(document).ready(function ($) {
	
$.typeahead({
    input: '.via',
    order: "desc",
    template: "{{display}} <small style='color:#999;'>Milano</small>",
    emptyTemplate: 'Siamo spiacenti, via non servita',
    source: {
        country: {
            ajax: {
                url: "../wp-content/plugins/woocommerce-street-filter/vie-json.php",
                path: "data.country"
            }
        }
    },
    callback: {
        onClickAfter: function (node, a, item, event) {
 
            event.preventDefault();
 
            $('#result-container').text('pippo');
 
        },
    }
});
})