      <form id="form-country_v1" name="form-country_v1" action="<?php echo get_permalink(get_option( "wsf_reg_page" )); ?>">
    <div class="typeahead__container">
        <div class="typeahead__field">
 
            <span class="typeahead__query">
                <input class="via" name="via" type="search" placeholder="Search" autocomplete="off">
            </span>
            <span class="typeahead__button">
                <button type="submit">
                    <i class="typeahead__search-icon"></i>
                </button>
            </span>
 
        </div>
    </div>
</form>