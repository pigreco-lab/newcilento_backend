	<?php global $woocommerce;
	$checkout=$woocommerce->checkout;
	$site=get_bloginfo('name');
	?>
<div class="registration-form woocommerce">

	<?php 
	$ers='';
	if(isset($_POST['action'])) {
		if ( isset( $_POST['billing_first_name'] ) && empty( $_POST['billing_first_name'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('First Name','woocommerce'), __('is a required field.','woocommerce'));
			//$validation_errors->add( 'billing_first_name_error', __( '<strong>Error</strong>: First name is required!', 'woocommerce' ) );
		}

		if ( isset( $_POST['billing_last_name'] ) && empty( $_POST['billing_last_name'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('Last Name','woocommerce'), __('is a required field.','woocommerce'));		}

		if ( isset( $_POST['billing_company'] ) && empty( $_POST['billing_company'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('Billing company','woocommerce'), __('is a required field.','woocommerce'));		}

		if ( isset( $_POST['pml_piva'] ) && empty( $_POST['pml_piva'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('VAT number','woocommerce-discounted-registration'), __('is a required field.','woocommerce'));
		}

		if ( isset( $_POST['billing_address_1'] ) && empty( $_POST['billing_address_1'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('Billing address','woocommerce'), __('is a required field.','woocommerce'));
		}

		if ( isset( $_POST['shipping_address'] ) && empty( $_POST['shipping_address'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('Shipping address','woocommerce-discounted-registration'), __('is a required field.','woocommerce'));
		}

		if ( isset( $_POST['billing_email'] ) && empty( $_POST['billing_email'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('Email','woocommerce'), __('is a required field.','woocommerce'));
		}

		// check if user email exists
		if ( isset( $_POST['billing_email'] ) && email_exists( $_POST['billing_email'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('An account is already registered with your email address. Please login.','woocommerce'), __('If you are not allowed yet.','woocommerce-discounted-registration'));
		}

		if ( isset( $_POST['billing_phone'] ) && empty( $_POST['billing_phone'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('Phone','woocommerce'), __('is a required field.','woocommerce'));
		}
		if ( isset( $_POST['billing_city'] ) && empty( $_POST['billing_city'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('City','woocommerce'), __('is a required field.','woocommerce'));
		}
		if ( isset( $_POST['billing_state'] ) && empty( $_POST['billing_state'] ) ) {
			$ers.= sprintf("<li>%s %s</li>",__('State','woocommerce'), __('is a required field.','woocommerce'));
		}
		if ( isset( $_POST['billing_postcode'] ) && empty( $_POST['billing_postcode'] ) ) {
			$ers.= sprintf("<li>%s</li>",__('Please enter a valid postcode/ZIP.','woocommerce'));
		}

		if ($ers!='') {
			echo '<ul class="woocommerce-error">'.$ers.'</ul>';
		} else {
			$customer_id = wc_create_new_customer( $_POST['billing_email'],$_POST['account_username'],$_POST['account_password']);
				// WordPress default first name field.
			update_user_meta( $customer_id, 'first_name', sanitize_text_field( $_POST['billing_first_name'] ) );

			// WooCommerce billing and shipping first name.
			update_user_meta( $customer_id, 'billing_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
			update_user_meta( $customer_id, 'shipping_first_name', sanitize_text_field( $_POST['billing_first_name'] ) );
						// WordPress default last name field.
			update_user_meta( $customer_id, 'last_name', sanitize_text_field( $_POST['billing_last_name'] ) );

			// WooCommerce billing and shipping last name.
			update_user_meta( $customer_id, 'billing_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );
			update_user_meta( $customer_id, 'shipping_last_name', sanitize_text_field( $_POST['billing_last_name'] ) );

			update_user_meta( $customer_id, 'billing_phone', sanitize_text_field( $_POST['billing_phone'] ) );
			update_user_meta( $customer_id, 'billing_email', sanitize_text_field( $_POST['billing_email'] ) );
			update_user_meta( $customer_id, 'shipping_phone', sanitize_text_field( $_POST['billing_phone'] ) );
			update_user_meta( $customer_id, 'billing_city', sanitize_text_field( $_POST['billing_city'] ) );
			update_user_meta( $customer_id, 'shipping_city', sanitize_text_field( $_POST['billing_city'] ) );
			update_user_meta( $customer_id, 'billing_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
			update_user_meta( $customer_id, 'billing_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );
			update_user_meta( $customer_id, 'shipping_address_1', sanitize_text_field( $_POST['billing_address_1'] ) );
			update_user_meta( $customer_id, 'shipping_address_2', sanitize_text_field( $_POST['billing_address_2'] ) );

			$random_code = wp_generate_password( $length=5, $include_standard_special_chars=false );

			update_user_meta( $customer_id, 'smscode', $random_code );

/*			$subject = sprintf(__('New registration request from %s','woocommerce-discounted-registration'),$site);//'Nuova richiesta di registrazione al sito Ingrosso Primaluce';
			$basemessage = sprintf(__("The company %s in the person of %s has applied for inclusion as dealer.%sTo verify the data and confirm the membership %s click here %s",'woocommerce-discounted-registration'),$_POST['billing_first_name'].' '.$_POST['billing_last_name'],'<br /><br />','<a href="http://ingrosso.primalucelampadari.it/gestione-registrazione/?user='.$customer_id.'">','</a>');


			$mailer = $woocommerce->mailer();

			$to=$mailer->emails['WC_Email_New_Order']->recipient;

			  $message = $mailer->wrap_message(
            // Message head and message body.
            sprintf(__('New registration at %s','woocommerce-discounted-registration'),$site), $basemessage
            );

		  $mailer->send( $to, $subject, $message);

		  	$cus_subject = sprintf(__('registration request to the site %s received','woocommerce-discounted-registration'),$site);
			$cus_message = sprintf(__("Dear% s, <br> your application as a dealer on %s site was received. <br /> <br /> Please note that you will have to wait  the confirmation email that will be sent, after appropriate controls, directly by the staff. You can then change your password. <br /> <br /> Thank you for your confidence",'woocommerce-discounted-registration'),$_POST['billing_first_name'].' '.$_POST['billing_last_name'],$site);

				  $cus_message = $mailer->wrap_message(
            // Message head and message body.
            sprintf(__('Confirmation of receipt subscription to the website %s','woocommerce-discounted-registration'),$site), $cus_message
             );

			$mailer->send( $_POST['billing_email'], $cus_subject, $cus_message);*/

			do_action( 'wsf_after_registration',$_POST['billing_email'], $_POST['billing_phone'], $random_code);

			echo sprintf('<ul class="woocommerce-info"><li>%s</li></ul>',__('Registration ready','woocommerce-discounted-registration'));
		}
	}
	  ?>




	<form action="#" method="post" class="register">



		<?php do_action( 'woocommerce_register_form_start' ); ?>
		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>
<div class="woocommerce-billing-fields">
<?php 
	$checkout->checkout_fields['billing']['billing_company']['required']=true;
	$checkout->checkout_fields['billing']['billing_address_2']['placeholder']='Specifica piano, ecc.';
 ?>
		<?php foreach ($checkout->checkout_fields['billing'] as $key => $field) : ?>
		
<?php 

	if ($key=='billing_address_1') {
			woocommerce_form_field( $key, $field,$_GET["via"] );
			continue;
		}
	// rimuove alcuni campi dal form	
	if ($key=='billing_company' || $key=='billing_postcode' || $key=='billing_state' || $key=='billing_country') continue;

		

		?>
		<?php woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); ?>
		<?php endforeach; ?>
		<?php foreach ($checkout->checkout_fields['account'] as $key => $field) : 
			woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
			endforeach; ?>
</div>
		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>

		<!-- Spam Trap -->
		<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

		<?php do_action( 'woocommerce_register_form' ); ?>
		<?php do_action( 'register_form' ); ?>

		<p class="form-row">
			<?php 

				 wp_nonce_field( 'woocommerce-edit_address' ); ?>
			<input type="hidden" name="action" value="edit_address" />

			<input type="submit" class="button wc-forward" name="register" value="<?php _e( 'Register', 'woocommerce' ); ?>" />
		</p>

		<?php do_action( 'woocommerce_register_form_end' ); ?>

	</form>

</div>