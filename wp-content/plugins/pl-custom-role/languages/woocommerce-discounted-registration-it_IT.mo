��          �            h  6   i     �     �     �  %  �          #      :  D   [     �     �  �   �  
   K  ,   V  �  �  .        <     R     a  {  �     �     	  &   2	  F   Y	     �	     �	  �   �	     ]
  .   i
                                                  
         	                     Confirmation of receipt subscription to the website %s Custom discount Customer discount Data changes made successfully Dear% s, <br> your application as a dealer on %s site was received. <br /> <br /> Please note that you will have to wait  the confirmation email that will be sent, after appropriate controls, directly by the staff. You can then change your password. <br /> <br /> Thank you for your confidence If you are not allowed yet. New registration at %s New registration request from %s Registration ready, you will be enabled after the appropriate checks Shipping address Standard discount The company %s in the person of %s has applied for inclusion as dealer.%sTo verify the data and confirm the membership %s click here %s VAT number registration request to the site %s received Project-Id-Version: WooCommerce Registration and Discount
POT-Creation-Date: 2016-07-01 18:11+0100
PO-Revision-Date: 2016-07-01 18:39+0100
Last-Translator: 
Language-Team: 
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.3
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Richiesta di registrazione al sito %s ricevuta Sconto personalizzato Sconto cliente Modifiche salvate con successo Gentile %s, <br> la sua richiesta di iscrizione come rivenditore sul sito %s è stata ricevuta.<br /><br />Le ricordiamo che per poter accedere dovrà attendere la mail di conferma che le verrà inviata, dopo le opportune verifiche, direttamente dallo staff di Primaluce Lampadari. Successivamente potrà modificare la sua password. <br /><br /> Grazie per la fiducia accordataci Se non sei ancora abilitato Nuova registrazione su %s Nuova richiesta di registrazione da %s Registrazione effettuata, verrai abilitato dopo le opportune verifiche Indirizzo spedizione Sconto standard L'azienda %s nella persona di %s ha chiesto di essere iscritto come rivenditore.%sPer verificare i dati e confermare la sua iscrizione %s clicca qui %s Partita IVA Richiesta di registrazione al sito %s ricevuta 