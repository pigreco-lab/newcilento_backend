<?php
/**
 * Template Name: Luoghi
 *
 * @package Rocked-child
 */

get_header(); 

  global $wp_query;
$pn=$wp_query->query_vars['accomodation-in'];

$idpn=get_page_by_path( $pn, ARRAY_N, 'place' );

$args = array(
	'post_type' => 'accomodation',
	'meta_query' => array(
		array(
			'key' => 'placename',
			'value' => $idpn[0],
			'compare' => '='
		)
	)
);

$r = get_posts( $args );


?>


		<div class="col-sm-12">
		<h1>Accomodations in <?php echo get_the_title($idpn[0]);	 ?></h1>
</div>

		
		<?php if ( $r ) { ?>

		     
		<div class="employees-area clearfix">
				<?php //Get the custom field values

			foreach ( $r as $post ) : 
		$term = get_the_terms( $post->ID, 'type' );
					$tipostru = $term[0]->slug;
					$numstan = get_post_meta($post->ID, 'numero_stanze', true );
					$regione  = get_post_meta( $post->ID, 'regione', true );
					$localita = get_post_meta( get_the_ID(), 'placename', true );
					$tipoicon= array(
        'beb' => 'beb',
        'hot' => 'hotel',
        'cav' => 'casavacanza',
        'aff' => 'citywalls',
        'cam' => 'villaggio',
        'agr' => 'agritourism',
        'vil' => 'agritourism'

            );

?>
				<div class="roll-team">
					<?php if ( has_post_thumbnail($post->ID)) { ?>
					<div class="photo">
						<div class="overlay">
							<div class="struicon"><?php echo '<img src="'.get_stylesheet_directory_uri().'/images/'.$tipoicon[$tipostru].'.png" />' ?></div>
							<div class="acc_content">
								<p class="text"><?php echo wp_kses_post(wp_trim_words( get_excerpt_by_id($post->ID),20,'' ));?></p>
							</div>
						</div>
						<?php the_post_thumbnail('scrolldim'); ?>
					</div>
					<?php } ?>
					<div class="info">
								<span><?php  echo $term[0]->name; ?></span>
				        	<h3><a href="<?php echo esc_url(get_permalink($post->ID)); ?>"><?php echo $post->post_title; ?></a></h3>
				        	<br>
						<div class="col-sm-12"><?php echo esc_html(get_the_title($localita)); ?></div>
						
					</div>
				</div>
			<?php endforeach; ?>
	</div>
		<?php } else { ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php }; ?>


<?php get_footer(); ?>
