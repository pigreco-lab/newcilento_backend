<?php

// image size per mappa

add_image_size( 'mapthumb', 120, 80, true );
add_image_size( 'scrolldim', 360, 240, true );
//add_image_size( 'fullac', 960, 720, true );

global $tipo;

$tipo= array(
        'beb' => 'B&B',
        'hot' => 'Hotel',
        'cav' => 'Casa Vacanza',
        'aff' => 'Affittacamere',
        'cam' => 'Camping',
        'cav' => 'Casa Vacanze'

            );

global $regio;

$regio= array(

    "valdaosta" => "Valle d\'Aosta",
    "piemonte" => "Piemonte",
    "liguria" => "Liguria",
    "lombardia" => "Lombardia",
    "veneto" => "Veneto",
    "trentino" => "Trentino Alto Adige",
    "friuli" => "Friuli Venezia Giulia",
    "emiliaromagna" => "Emilia Romagna",
    "toscana" => "Toscana",
    "umbria" => "Umbria",
    "marche" => "Marche",
    "lazio" => "Lazio",
    "abruzzo" => "Abruzzo",
    "molise" => "Molise",
    "campania" => "Campania",
    "puglia" => "Puglia",
    "basilicata" => "Basilicata",
    "calabria" => "Calabria",
    "sicilia" => "Sicilia",
    "sardegna" => "Sardegna"

            );


add_action( 'wp_enqueue_scripts', 'enqueue_theme_style' );
function enqueue_theme_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'colorboxcss', get_stylesheet_directory_uri().'/css/colorbox.css' );

}

function my_acf_google_map_api( $api ){
    
    $api['key'] = 'AIzaSyCwYIMoPywiCLJCxBrHamqFlNNNs8mN5Zk';
    
    return $api;
    
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function frontpage_script() {
    if( is_front_page() )
    {
           wp_enqueue_style( 'acf-map', get_stylesheet_directory_uri() . '/css/acf-map.css' );
               wp_enqueue_script( 'gmapjs','https://maps.googleapis.com/maps/api/js?key=AIzaSyCwYIMoPywiCLJCxBrHamqFlNNNs8mN5Zk');
    wp_enqueue_script( 'acfmapjs', get_stylesheet_directory_uri() . '/js/acf-homemap.js', array( 'jquery' ), '20130508', true );

    }

}
add_action( 'wp_enqueue_scripts', 'frontpage_script' );

function single_acc_place_script() {
    if( is_singular( 'place' )||is_singular( 'accomodation' ))
    {
               wp_enqueue_script( 'gmapjs','https://maps.googleapis.com/maps/api/js?key=AIzaSyCwYIMoPywiCLJCxBrHamqFlNNNs8mN5Zk');
               wp_enqueue_script( 'lightslider',get_stylesheet_directory_uri(). '/js/lightslider.min.js', array( 'jquery' ), '20130508', true );
                wp_enqueue_style( 'lightslidercss', get_stylesheet_directory_uri().'/css/lightslider.min.css' );

    }

}
add_action( 'wp_enqueue_scripts', 'single_acc_place_script' );



function child_custom_script() {

    wp_enqueue_script( 'colorbox', get_stylesheet_directory_uri() . '/js/jquery.colorbox-min.js', array( 'jquery' ) );
    wp_enqueue_script( 'childscript', get_stylesheet_directory_uri() . '/js/childscript.js', array( 'jquery' ) );


}
add_action( 'wp_enqueue_scripts', 'child_custom_script' );

function add_itrav_map() {

global $wpdb;
    $query = "SELECT post_id,meta_value FROM ".$wpdb->prefix."postmeta WHERE meta_key='acc_pos'";
    $results = $wpdb->get_results($query, ARRAY_A);

    $r='<div class="acf-map">';

    foreach ( $results as $res ) {

        $pid=$res['post_id'];

        if(get_post_type($pid)!='accomodation' && get_post_status($pid)!='publish') continue;

        $types=get_the_terms( $pid, 'type' );

        $type = $types[0]->slug;

        //$type=$wpdb->get_var( "SELECT meta_value FROM ".$wpdb->prefix."postmeta WHERE meta_key='acc_type' AND post_id=".$pid );
        
            $a = unserialize($res['meta_value']);
            $r.='<div class="marker" data-lat="'.$a['lat'].'" data-lng="'.$a['lng'].'" data-type="'.$type.'">
            <div class="text-center">
                <h4>'.get_the_title($pid).'</h4>
                <p>'.get_the_post_thumbnail($pid,'mapthumb').'</p>
                <p>'.get_excerpt_by_id($pid).'</p>
                <button type="button" class="btn btn-primary btn-sm"><a href="'.get_permalink($pid).'">View</a></button>
                </div>
            </div>';
    }
    $r.='</div>';

    $r.='<div class="regionfield"><select id="region">
        <option value="all">All Italy</option>
        <option value="valdaosta">Valle d\'Aosta</option>
        <option value="piemonte">Piemonte</option>
        <option value="liguria">Liguria</option>
        <option value="lombardia">Lombardia</option>
        <option value="veneto">Veneto</option>
        <option value="trentino">Trentino Alto Adige</option>
        <option value="friuli">Friuli Venezia Giulia</option>
        <option value="emiliaromagna">Emilia Romagna</option>
        <option value="toscana">Toscana</option>
        <option value="umbria">Umbria</option>
        <option value="marche">Marche</option>
        <option value="lazio">Lazio</option>
        <option value="abruzzo">Abruzzo</option>
        <option value="molise">Molise</option>
        <option value="campania">Campania</option>
        <option value="puglia">Puglia</option>
        <option value="basilicata">Basilicata</option>
        <option value="calabria">Calabria</option>
        <option value="sicilia">Sicilia</option>
        <option value="sardegna">Sardegna</option>
      </select></div>';

   return $r;

}

        add_shortcode( 'itrav_map',  'add_itrav_map' );




add_action( 'wp_enqueue_scripts', 'genesis_map_icon_fonts' );
function genesis_map_icon_fonts() {
wp_enqueue_style( 'map-fonts', get_stylesheet_directory_uri() . '/fonts/map-icons.css');
}


/******* ADD CUSTOM ENDPOINT FOR REGION AND ACCOMODATION-IN*****


/**
*   Add the 'region' query variable so Wordpress
*   won't mangle it.
*/
function add_query_vars($vars){
    $vars[] = "region";
    $vars[] = "accomodation-in";
    $vars[] = "towns";
    return $vars;
}
add_filter( 'query_vars', 'add_query_vars');

function makeplugins_add_json_endpoint() {
    add_rewrite_endpoint('region', EP_ROOT );
    add_rewrite_endpoint('accomodation-in', EP_ROOT );
    add_rewrite_endpoint('towns', EP_ROOT );
}
add_action( 'init', 'makeplugins_add_json_endpoint',999 );


function region_template($template){
    global $wp_query;
//wp_die(var_dump($wp_query->query_vars));
    // If the 'region' endpoint isn't appended to the URL,
    // don't do anything and return
    if(isset( $wp_query->query_vars['region'] )) {
        $new_template = locate_template( array( 'region-template.php' ) );
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }
    if(isset( $wp_query->query_vars['towns'] )) {
        $new_template = locate_template( array( 'towns-template.php' ) );
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }

    if(isset( $wp_query->query_vars['accomodation-in'] )) {
        $new_template = locate_template( array( 'accomodation-in-template.php' ) );
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }

    return $template;
}
add_filter( 'template_include', 'region_template',99 );


//Adds a custom function for get_excerpt by id

if (!function_exists(get_excerpt_by_id)) {
    /**
     * Returns excerpt by ID. WP_Post->post_excerpt if it presents, wp_trim_words on other case.
     * @param  mixed  $post  WP_Post object or post ID. Accepts WP_Post object or post ID (integer or string);
     * @return string        Excerpt
     */
    function get_excerpt_by_id($post) {
        $return_excerpt = function($post) {
            if ($post->post_excerpt == '')
                return wp_trim_words($post->post_content, apply_filters( 'excerpt_length', 55 ), apply_filters( 'excerpt_more', ' ' . '[&hellip;]' ));
            return $post->post_excerpt;
        };
        if (!is_object($post)) {
            $post = get_post($post);
        }
        return apply_filters('the_excerpt', $return_excerpt($post));
    }
}

function pre_get_posts_hook($query) { 
//  if (  $query->is_main_query() && $query->is_archive() ) {
    if (  $query->is_tax('type') || $query->is_tax('place')  ) {
        $query->set( 'orderby', 'rand' );
      }
}
add_filter('pre_get_posts', 'pre_get_posts_hook' );

add_action( 'init', 'my_custom_post_type_rest_support', 25 );

function my_custom_post_type_rest_support() {

    global $wp_post_types;

//be sure to set this to the name of your post type!
    $post_type_name = 'accomodation';

    if( isset( $wp_post_types[ $post_type_name ] ) ) {

        $wp_post_types[$post_type_name]->show_in_rest = true;
// Optionally customize the rest_base or controller class
        $wp_post_types[$post_type_name]->rest_base = $post_type_name;
        $wp_post_types[$post_type_name]->rest_controller_class = 'WP_REST_Posts_Controller';

    }

}

$post_type = "accomodation";
function my_rest_prepare_post($data, $post, $request) {
    $_data = $data->data;
    $fields = get_fields($post->ID);
    foreach ($fields as $key => $value) {
        $_data[$key] = get_field($key, $post->ID);
    }
    //The helper function acf_photo_gallery contains array. Append this array to your data array
    $_data['acf_photo_gallery'] = acf_photo_gallery('gallery', $post->ID);
    $data->data = $_data;
    return $data;
}
add_filter("rest_prepare_{$post_type}", 'my_rest_prepare_post', 10, 3);
        ?>