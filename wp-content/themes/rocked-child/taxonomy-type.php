<?php
/**
 * Services archives template
 *
 * @package Rocked
 */

get_header(); ?>


		<div class="col-sm-12">
		<h1><?php 
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		echo $term->name;
		 ?></h1>
<p><?php echo term_description(); ?></p>
</div>
</div>
		<?php if ( have_posts() ) : ?>

		<div class="employees-area clearfix">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php //Get the custom field values
					$tipostru = $term->slug;
					$numstan = get_post_meta( get_the_ID(), 'numero_stanze', true );
					$regione  = get_post_meta( get_the_ID(), 'regione', true );
					$localita = get_post_meta( get_the_ID(), 'placename', true );

					$tipoicon= array(
        'beb' => 'beb',
        'hot' => 'hotel',
        'cav' => 'casavacanza',
        'aff' => 'citywalls',
        'cam' => 'villaggio',
        'agr' => 'agritourism',
        'vil' => 'agritourism'

            );
				?>
				<div class="roll-team">
					<?php if ( has_post_thumbnail() ) : ?>
					<div class="photo">
						<div class="overlay">
							<div class="struicon"><?php echo '<img src="'.get_stylesheet_directory_uri().'/images/'.$tipoicon[$tipostru].'.png" />' ?></div>
							<div class="acc_content">
								<p class="text"><?php echo wp_kses_post(wp_trim_words( get_the_excerpt(), 10, '' ));?></p>

							</div>
						</div>
						<?php the_post_thumbnail('scrolldim'); ?>
					</div>
					<?php endif; ?>
					<div class="info">
								<span><?php  echo $term->name; ?></span>
				        	<h3><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a></h3>
				        	<br>
						<div class="col-sm-12"><?php echo esc_html(get_the_title($localita)); ?></div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>	
			<?php the_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>


<?php get_footer(); ?>
