<?php
/**
 * The template for displaying all single posts.
 *
 * @package Rocked
 */

get_header(); ?>



		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'accomodation' ); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
