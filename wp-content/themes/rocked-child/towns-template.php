<?php
/**
 * Template Name: Regioni
 *
 * @package Rocked-child
 */

get_header(); 

  global $wp_query;
$twn=$wp_query->query_vars['towns'];

$args = array(
	'post_type' => 'place',
	'numberposts'       => -1,
	'orderby' => 'rand',
	'meta_query' => array(
		array(
			'key' => 'regione',
			'value' => $twn,
			'compare' => '='
		)
	)
);

$r = get_posts( $args );

global $regio;


?>


		<div class="col-sm-12">

		<h1><?php echo esc_html($regio[$reg]); ?></h1>
</div>

		
		<?php if ( $r ) { ?>

		     
		<div class="employees-area clearfix">


				<?php
			foreach ( $r as $post ) : 
		$term = get_the_terms( $post->ID, 'type' );

		 
				 //Get the custom field values
					$tipostru = $term[0]->slug;
					$regione  = get_post_meta( $post->ID, 'regione', true );
							
?>
				<div class="roll-team">
					<?php if ( has_post_thumbnail($post->ID)) { ?>
					<div class="photo">
						<div class="overlay">
							<div class="acc_content">
								<p class="text"><?php echo wp_kses_post(wp_trim_words( get_excerpt_by_id($post->ID),20,'' ));?></p>
							</div>
						</div>
						<?php the_post_thumbnail('scrolldim'); ?>
					</div>
					<?php } ?>
					<div class="info">
								<span><?php  echo $term[0]->name; ?></span>
				        	<h3><a href="<?php echo esc_url(get_permalink($post->ID)); ?>"><?php echo $post->post_title; ?></a></h3>
				        	<br>
						<div class="col-sm-12"><?php //echo esc_html(get_the_title($localita)); ?></div>
						
					</div>
				</div>
			<?php endforeach; ?>
	</div>
		<?php } else { ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php }; ?>


<?php get_footer(); ?>
