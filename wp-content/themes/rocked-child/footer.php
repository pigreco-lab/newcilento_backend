</div>
</div>
</div>


  <footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-3 col-lg-3">

          <?php dynamic_sidebar('footer-1'); ?>

      </div>
      <div class="col-sm-3 col-lg-3">
        <div class="widget">
          <h4>Informazioni</h4>
          <?php wp_nav_menu( array('theme_location' => 'footer') ); ?>
        </div>
        
      </div>
      <div class="col-sm-3 col-lg-3">
        <div class="widget">
<?php dynamic_sidebar('footer-2'); ?>
        </div>
      </div>
      <div class="col-sm-3 col-lg-3">
          <?php dynamic_sidebar('footer-3'); ?>
      </div>
    </div>
  </div>
  </footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>


<?php wp_footer(); ?>
</body>
</html>
