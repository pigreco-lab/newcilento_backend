
(function($) {

/*
*  new_map
*
*  This function will render a Google Map onto the selected jQuery element
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$el (jQuery element)
*  @return	n/a
*/

function new_map( $el ) {
	
	// var
	var $markers = $el.find('.marker');
	
	
	// vars
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.ROADMAP
	};
	
	
	// create map	        	
	var map = new google.maps.Map( $el[0], args);
	
	
	// add a markers reference
	map.markers = [];
	
	
	// add markers
	$markers.each(function(){
		
    	add_marker( $(this), map );
		
	});
	
	
	// center map
	center_map( map );
	
	
	// return
	return map;
	
}

/*
*  add_marker
*
*  This function will add a marker to the selected Google Map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	$marker (jQuery element)
*  @param	map (Google Map object)
*  @return	n/a
*/

function add_marker( $marker, map ) {

	// var
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

	  var iconBase = 'wp-content/themes/rocked-child/images/';
        var icons = {
          hot: {
            icon: iconBase + 'hotel.png'
          },
          cav: {
            icon: iconBase + 'casavacanza.png'
          },
          cam: {
            icon: iconBase + 'camping.png'
          },
          agr: {
            icon: iconBase + 'agritourism.png'
          },
          aff: {
            icon: iconBase + 'hotel.png'
          },
           beb: {
            icon: iconBase + 'beb.png'
          },
           vil: {
            icon: iconBase + 'villaggio.png'
          }
        };

	// create marker
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
		//icon: icons['hotel'].icon
		icon: icons[$marker.attr('data-type')].icon
	});

	// add to array
	map.markers.push( marker );

	// if marker contains HTML, add it to an infoWindow
	if( $marker.html() )
	{
		// create info window
		var infowindow = new google.maps.InfoWindow({
			content		: $marker.html(),
      maxWidth: 200
		});

		// show info window when marker is clicked
		google.maps.event.addListener(marker, 'click', function() {

			infowindow.open( map, marker );

		});
	}

}

/*
*  center_map
*
*  This function will center the map, showing all markers attached to this map
*
*  @type	function
*  @date	8/11/2013
*  @since	4.3.0
*
*  @param	map (Google Map object)
*  @return	n/a
*/

function center_map( map ) {

	// vars
	var bounds = new google.maps.LatLngBounds();

	// loop through all markers and create bounds
	$.each( map.markers, function( i, marker ){

		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

		bounds.extend( latlng );

	});

	// only 1 marker?
	if( map.markers.length == 1 )
	{
		// set center of map
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		// fit to bounds
		map.fitBounds( bounds );
	}

}



      var regions = {
        'campania': {
          center: {lat: 40.7293242, lng:  14.7993445},
          zoom: 8
        },
        'lazio': {
          center: {lat:  41.9027834, lng: 12.4963655},
          zoom: 8
        },
        'toscana': {
          center: {lat: 43.4449002, lng: 10.9574831},  
          zoom: 8
        },
        'liguria': {
          center: {lat: 44.4336014, lng: 8.80436123},  
          zoom: 8
        },
        'piemonte': {
          center: {lat: 45.0832738 , lng:  7.91089631},  
          zoom: 8
        },
        'valdaosta': {
          center: {lat: 45.7349551, lng: 7.31307620},  
          zoom: 10
        },
        'lombardia': {
          center: {lat: 45.4790670, lng: 9.84524329},    
          zoom: 8
        },
        'veneto': {
          center: {lat: 45.7623332, lng: 11.6909759},   
          zoom: 8
        },
        'trentino': {
          center: {lat: 46.4336662, lng: 11.1693295},  
          zoom: 9
        },
        'friuli': {
          center: {lat: 46.2259176, lng: 13.1033645},  
          zoom: 9
        },
        'emiliaromagna': {
          center: {lat: 44.5967607, lng: 11.2186395},   
          zoom: 8
        },
        'umbria': {
          center: {lat: 42.9380040, lng: 12.6216210},    
          zoom: 9
        },
        'marche': {
          center: {lat: 43.5058744, lng:  12.9896149},  
          zoom: 9
        },
        'abruzzo': {
          center: {lat: 42.1920119, lng: 13.7289167},     
          zoom: 9
        },
        'molise': {
          center: {lat: 41.6738864, lng: 14.7520938},      
          zoom: 10
        },
        'puglia': {
          center: {lat: 40.7928393, lng: 17.1011931},     
          zoom: 8
        },
        'basilicata': {
          center: {lat: 40.6430766, lng: 15.9699878},     
          zoom: 8
        },
        'calabria': {
          center: {lat: 39.3087713, lng: 16.3463791},       
          zoom: 9
        },
        'sicilia': {
          center: {lat: 37.5999938, lng:  14.0153556},     
          zoom: 8
        },
        'sardegna': {
          center: {lat: 40.1208751, lng:  9.01289259},
          zoom: 8
        }
      };
  // Set the country restriction based on user input.
      // Also center and zoom the map on the given country.
      function setRegion() {
        var regione = document.getElementById('region').value;
        if (regione == 'all') {
          map.setCenter({lat: 15, lng: 0});
          map.setZoom(2);
        } else {
          map.setCenter(regions[regione].center);
          map.setZoom(regions[regione].zoom);
        }
      }

         // Add a DOM event listener to react when the user selects a country.
        document.getElementById('region').addEventListener(
            'change', setRegion);
     

/*
*  document ready
*
*  This function will render each map when the document is ready (page has loaded)
*
*  @type	function
*  @date	8/11/2013
*  @since	5.0.0
*
*  @param	n/a
*  @return	n/a
*/
// global var
var map = null;

$(document).ready(function(){

	$('.acf-map').each(function(){

		// create map
		map = new_map( $(this) );

	});

});

})(jQuery);
