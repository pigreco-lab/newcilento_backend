<?php
/**
 * @package Rocked
 */
global $tipo;
$aid=get_the_id();

        $types=get_the_terms( $aid, 'type' );

        $type = $types[0]->slug;

        $tipo= array(
        'beb' => 'B&B',
        'hot' => 'Hotel',
        'cav' => 'Holiday House',
        'aff' => 'Rent-a-Room',
        'cam' => 'Camping',
        'vil' => 'Village',
        'agr' => 'Farm House',

            );

?>
<div class="container">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<div class="row">
	<div class="col-sm-8 col-md-8 col-xs-12">
		
<ul id='lightSlider'>
        <?php
    //Get the images ids from the post_metadata
    $images = acf_photo_gallery('gallery', $aid);

    //Check if return array has anything in it
    if( count($images) ):
        //Cool, we got some data so now let's loop over it
        foreach($images as $image):
            $title = $image['title']; //The title
            $caption= $image['caption']; //The caption
            $full_image_url= $image['full_image_url']; //Full size image url
            $thumb_image_url = acf_photo_gallery_resize_image($full_image_url, 960, 720); //Resized size to 262px width by 160px height image url
            $url= !empty($image['url']) ? $image['url'] : $full_image_url; //Goto any link when clicked
            $target= $image['target']; //Open normal or new tab
            $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
            $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)
?>
          <?php //echo var_dump($full_image_url); ?>
          <li data-thumb="<?php echo $thumb_image_url; ?>">
             <img src="<?php echo $thumb_image_url; ?>" alt="<?php echo $title; ?>" title="<?php echo $title; ?>">
          </li>
    

<?php endforeach; endif; ?>
  </ul><br />
	</div>
	<div class="col-sm-4 col-md-4 col-xs-12">	
		<header class="entry-header">
			<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
			<?php
			$plid=get_post_meta($aid,'placename', true);
			 echo '<a href="'.get_the_permalink($plid).'">'.get_the_title($plid).'</a>';
			 ?>

			<?php if (get_theme_mod('hide_meta_single') != 1 ) : ?>
			<div class="post-meta">
				</div><!-- .entry-meta -->
			<?php endif; ?>

			<div class="row">
				<div class="col-sm-6">
								 <i class="map-icon-big map-icon-florist"></i><?php echo $tipo[$type] ?><br /><br />

				</div>
				<div class="col-sm-6">
				 <i class="map-icon-big map-icon-lodging"></i><?php echo get_post_meta($aid,'numero_stanze', true) ?> rooms / flats<br><br />
				</div>
			</div>
		</header><!-- .entry-header -->



		<div class="entry-content">

		<br>
			<?php the_excerpt(); ?>

		</div><!-- .entry-content -->

		<?php if (get_theme_mod('hide_meta_single') != 1 ) : ?>
		<footer class="entry-footer">
			<?php rocked_entry_footer(); ?>
		</footer><!-- .entry-footer -->
		<?php endif; ?>
		</div>
	</div>
	<div class="row">


<div class="acccontent">		<?php the_content(); ?></div>

<div class="accservices"><h2>The services</h2><hr><?php echo get_post_meta($aid,'servizi', true) ?><br><br></div>
<div class="accterritory"><h2>Territory</h2><hr><?php echo get_post_meta($aid,'territorio', true) ?><br><br></div>

 <?php   	$latln= get_post_meta($aid,'acc_pos', true);

 if ($latln) { ?>
  <div id="map" style="height: 400px;"></div>
    <script>

      function initMap() {
        var myLatLng = {<?php echo 'lat: '.$latln['lat'].', lng: '.$latln['lng'];?>};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 11,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
			
          title: '<?php the_title()  ?>'
        });
      }

      jQuery(document).ready(function($) {
      	initMap();

        $('#lightSlider').lightSlider({
          auto:true,
            gallery: true,
            item: 1,
            loop:true,
            slideMargin: 0,
            thumbItem: 9
        });
      })
    </script>
<?php } ?>
	</div>

</article><!-- #post-## -->
	</div>