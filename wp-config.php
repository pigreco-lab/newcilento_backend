<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'NgQF1CaJPgApgB66Re6sXvUHaa86mXdRuVzYOjtgohgH0fNEB52+bDb7kPgYkI/VBJI2bT8yS22HnPJ/ERzhJQ==');
define('SECURE_AUTH_KEY',  'GUBHveZV8PsjWrlplzELRX6epkZedKVLcemxw0NmtmEYW+F1h2rOBfzSKye98g0mdHYVSi4o6Ef0xgRYwgy9cQ==');
define('LOGGED_IN_KEY',    'IKJGcD44JTfO5Eh+2FfxtcPgylNs4E7Y8vO1rL/xW1rDZP8EQ2iN5RIykVrXsks7lsojkDdZhRO8PzK8qr1T/Q==');
define('NONCE_KEY',        'uLeX43U5FNb3g3bGBlTDZzPtOKcIEfdq3HBp3TkdSfjddYUcQx8/8Z5MqQ4Vm6OqMl9XY4krhM2TLoag5VpsYg==');
define('AUTH_SALT',        '2IJEAkTNPNfaub/BGoWLlRPcH1ueCT/fzuObjGHT4FmRnpwGEodjXJzqF4dBRDQRjiBYZKxVc6M8a+quMozzKw==');
define('SECURE_AUTH_SALT', 'CjGsWUTyyNY/b6MjexhMqTNO0S659/0Ln05TcyEpqVsW4FTmadbwPmkrDsEWZBhU83VwgY5vgFKhR/FR9kSXoQ==');
define('LOGGED_IN_SALT',   '5hFaXyabLmuWBygVW3IR0YeJdLwY2dFBOvN+XrQYIx21zeFJtE6D3k6PKT3Xrde3rqKsK8TfedtmIwzfxiJQnw==');
define('NONCE_SALT',       'JexhK8Bc2Y5HPK+nv6aQ/Y/le3lL2/OgQkhKRwf2wjVgpbvcqhjGGK11RIcEEYrs/IAz+lAFwqrxrffj3quzEA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
